#include <stdio.h>


int TasksLength = 0;

#define task_type_void 0
#define task_params_length 10
#define task_type_string_buffer 1

struct Task {
    // 0 to 5 with zero being the highest priority
    int priority;
    int taskId;
    char ca1[100];
    int i1;

    // Function pointers
    int (*function)(int);
};

struct Task tasks[256];
int iparams[500] = {10};

void ProcessTasks() {
    int priority;
    int i = 0;

    priority = 5;
    while (priority >= 0) {
        i = mouse_possessed_task_id;
        if (left_clicked == TRUE &&
            mx > iparams[i * task_params_length + 0] &&
            mx < iparams[i * task_params_length + 0] + iparams[i * task_params_length + 2] &&
            my > iparams[i * task_params_length + 1] &&
            my < iparams[i * task_params_length + 1] + iparams[i * task_params_length + 3])
            break;

        for (i = 0; i < TasksLength; i++) {
            if (left_clicked == TRUE &&
                mx > iparams[i * task_params_length + 0] &&
                mx < iparams[i * task_params_length + 0] + iparams[i * task_params_length + 2] &&
                my > iparams[i * task_params_length + 1] &&
                my < iparams[i * task_params_length + 1] + iparams[i * task_params_length + 3]) {
                    tasks[mouse_possessed_task_id].priority = 0;
                    mouse_possessed_task_id = i;
                    tasks[i].priority = 2;
                    left_clicked = FALSE;
                }
        }
        priority--;
    }

    priority = 0;
    while (priority <= 5) {
        for (int i = 0; i < TasksLength; i++) {
            if (tasks[i].priority == priority) {
                tasks[i].function(tasks[i].taskId);
            }
        }

        priority++;
    }
}

int NullTask(int taskId) {
    return 0;
}

void CloseTask(int taskId) {
    tasks[taskId].function = &NullTask;
    iparams[taskId * task_params_length + 0] = 0;
    iparams[taskId * task_params_length + 1] = 0;
    iparams[taskId * task_params_length + 2] = 0;
    iparams[taskId * task_params_length + 3] = 0;
}

int ClearScreenTask(int taskId) {
    ClearScreen(181.0f / 255.0f * 16.0f, 232.0f / 255.0f * 32.0f, 255.0f / 255.0f * 16.0f);
    
    return 0;
}

int DrawMouseTask(int taskId) {
    DrawMouse(mx, my, 16, 100.0 / 255.0 * 32, 100.0 / 255.0 * 16);

    return 0;
}

/*int HandleKeyboardTask(int taskId) {
    char* characterBuffer = tasks[taskId].ca1;
    int* characterBufferLength = &tasks[taskId].i1;
    char character = ProcessScancode(Scancode);
    if (backspace_pressed == TRUE) {
        characterBuffer[*characterBufferLength - 1] = '\0';
        (*characterBufferLength)--;
        backspace_pressed = FALSE;
        Scancode = -1;
    }

    else if (character != '\0') {
        characterBuffer[*characterBufferLength] = character;
        characterBuffer[*characterBufferLength + 1] = '\0';
        (*characterBufferLength)++;
        Scancode = -1;
    }

    DrawString(getArialCharacter, font_arial_width, font_arial_height, characterBuffer, 50, 100, 16, 32, 16);
    return 0;
}*/

/*int ShellTask(int taskId) {
    int* r = &iparams[taskId * task_params_length + 4];
    int* g = &iparams[taskId * task_params_length + 5];
    int* b = &iparams[taskId * task_params_length + 6];

    int closeClicked = DrawWindow(
        &iparams[taskId * task_params_length + 0],
        &iparams[taskId * task_params_length + 1],
        &iparams[taskId * task_params_length + 2],
        &iparams[taskId * task_params_length + 3],
        *r,
        *g,
        *b,
        &iparams[taskId * task_params_length + 9],
        taskId);

    int x = iparams[taskId * task_params_length + 0];
    int y = iparams[taskId * task_params_length + 1];
    int width = iparams[taskId * task_params_length + 2];
    int height = iparams[taskId * task_params_length + 3];

    if(closeClicked == TRUE)
        CloseTask(taskId);

    char text[] = "Dark\0";
    char text1[] = "Light\0";

    if (DrawButton(x + 20, y + 20, 50, 20, 0, 32, 0,
        text, 16, 32, 16, taskId
    ) == TRUE) {
        *r = 0;
        *g = 0;
        *b = 0;
    }

    if (DrawButton(x + 100, y + 20, 50, 20, 0, 32, 0,
            text1, 16, 32, 16, taskId
    ) == TRUE) {
        *r = 16;
        *g = 31;
        *b = 16;
    }

    return 0;
}*/

#include "programs/calculator.c"
#include "programs/notes.c"
#include "programs/phone.c"
#include "programs/sms.c"
#include "programs/console.c"
#include "programs/files.c"
#include "programs/settings.c"


/*int BallTask(int taskId) {
    int x = iparams[taskId * task_params_length + 0];
    int y = iparams[taskId * task_params_length + 1];
    
    int width = iparams[taskId * task_params_length + 2];
    int height = iparams[taskId * task_params_length + 3];

    int closeClicked = DrawWindow(
        &iparams[taskId * task_params_length + 0],
        &iparams[taskId * task_params_length + 1],
        &iparams[taskId * task_params_length + 2],
        &iparams[taskId * task_params_length + 3],
        0, 0, 0,
        &iparams[taskId * task_params_length + 9],
        taskId);
    
    if(closeClicked == TRUE)
        CloseTask(taskId);

    iparams[taskId * task_params_length + 5] += iparams[taskId * task_params_length + 7];
    iparams[taskId * task_params_length + 6] += iparams[taskId * task_params_length + 8];

    if (iparams[taskId * task_params_length + 5] + 10 > iparams[taskId * task_params_length + 2] ||
        iparams[taskId * task_params_length + 5] - 10 < 0)
        iparams[taskId * task_params_length + 7] = -iparams[taskId * task_params_length + 7];

    if (iparams[taskId * task_params_length + 6] + 10 > iparams[taskId * task_params_length + 3] ||
        iparams[taskId * task_params_length + 6] - 10 < 20)
        iparams[taskId * task_params_length + 8] = -iparams[taskId * task_params_length + 8];

    DrawCircle(x + iparams[taskId * task_params_length + 5], y + iparams[taskId * task_params_length + 6], 10, 16, 32, 16);
}*/

int HeaderTask(int taskId) {
    char txt_debug[10] = "Debug\0";
    char txt_time[10] = "09:00 PM\0";

    VBEInfoBlock* VBE = (VBEInfoBlock*) VBEInfoAddress;

    DrawRect(0, 0, VBE->x_resolution, 20, 85 /2, 86, 91/2);
    DrawString(getArialCharacter, font_arial_width, font_arial_height, txt_debug,10, 0, 255, 255, 255);
    DrawString(getArialCharacter, font_arial_width, font_arial_height, txt_time,-80, 0, 255, 255, 255);
}

int TaskbarTask(int taskId) {
    VBEInfoBlock* VBE = (VBEInfoBlock*) VBEInfoAddress;
    
    int icon_y_pos = VBE->y_resolution-40;
    int icon_x_pos = VBE->x_resolution ;
    int i = iparams[taskId * task_params_length + 4];

    DrawRect(VBE->x_resolution / 4, VBE->y_resolution-70, 370, 60, 85 /2, 86, 91/2);

    // SHELL BTN
    char text[] = "Sett\0";
    if (DrawButton(170, icon_y_pos - 20, 50, 40, 211 /2, 196, 187 /2, text, 16, 32, 16, taskId) == TRUE) {
        tasks[TasksLength].priority = 0;
        tasks[TasksLength].taskId = TasksLength;
        tasks[TasksLength].function = &SettingsTask;
        iparams[TasksLength * task_params_length + 0] = i * 40;
        iparams[TasksLength * task_params_length + 1] = i * 40;
        iparams[TasksLength * task_params_length + 2] = 300;
        iparams[TasksLength * task_params_length + 3] = 300;
        iparams[TasksLength * task_params_length + 4] = 0;
        iparams[TasksLength * task_params_length + 5] = 0;
        iparams[TasksLength * task_params_length + 6] = 0;
        TasksLength++;
        iparams[taskId * task_params_length + 4]++;
    }

    // BALL BTN
    char text2[] = "Files\0";
    if (DrawButton(230, icon_y_pos - 20, 50, 40, 211 /2, 196, 187 /2, text2, 16, 32, 16, taskId) == TRUE) {
        tasks[TasksLength].priority = 3;
        tasks[TasksLength].taskId = TasksLength;
        tasks[TasksLength].function = &FilesTask;
        iparams[TasksLength * task_params_length + 0] = i * 40;
        iparams[TasksLength * task_params_length + 1] = i * 40;
        iparams[TasksLength * task_params_length + 2] = 300;
        iparams[TasksLength * task_params_length + 3] = 300;
        iparams[TasksLength * task_params_length + 4] = 0;
        iparams[TasksLength * task_params_length + 5] = 20;
        iparams[TasksLength * task_params_length + 6] = 30;
        iparams[TasksLength * task_params_length + 7] = 5;
        iparams[TasksLength * task_params_length + 8] = 5;
        TasksLength++;
    }
    
    // CALCULATOR BTN
    char text3[] = "Math\0";
    if (DrawButton(290, icon_y_pos - 20, 50, 40, 211 /2, 196, 187 /2, text3, 16, 32, 16, taskId) == TRUE) {
        tasks[TasksLength].priority = 3;
        tasks[TasksLength].taskId = TasksLength;
        tasks[TasksLength].function = &MathTask;
        iparams[TasksLength * task_params_length + 0] = i * 40;
        iparams[TasksLength * task_params_length + 1] = i * 40;
        iparams[TasksLength * task_params_length + 2] = 300;
        iparams[TasksLength * task_params_length + 3] = 300;
        iparams[TasksLength * task_params_length + 4] = 0;
        iparams[TasksLength * task_params_length + 5] = 20;
        iparams[TasksLength * task_params_length + 6] = 30;
        iparams[TasksLength * task_params_length + 7] = 5;
        iparams[TasksLength * task_params_length + 8] = 5;
        TasksLength++;
    }

    // NOTE BTN
    char text4[] = "Note\0";
    if (DrawButton(350, icon_y_pos - 20, 50, 40, 211 /2, 196, 187 /2, text4, 16, 32, 16, taskId) == TRUE) {
        tasks[TasksLength].priority = 3;
        tasks[TasksLength].taskId = TasksLength;
        tasks[TasksLength].function = &NoteTask;
        iparams[TasksLength * task_params_length + 0] = i * 40;
        iparams[TasksLength * task_params_length + 1] = i * 40;
        iparams[TasksLength * task_params_length + 2] = 300;
        iparams[TasksLength * task_params_length + 3] = 300;
        iparams[TasksLength * task_params_length + 4] = 0;
        iparams[TasksLength * task_params_length + 5] = 20;
        iparams[TasksLength * task_params_length + 6] = 30;
        iparams[TasksLength * task_params_length + 7] = 5;
        iparams[TasksLength * task_params_length + 8] = 5;
        TasksLength++;
    }

    // PHONE BTN
    char text6[] = "Phone\0";
    if (DrawButton(410, icon_y_pos - 20, 50, 40, 211 /2, 196, 187 /2, text6, 16, 32, 16, taskId) == TRUE) {
        tasks[TasksLength].priority = 3;
        tasks[TasksLength].taskId = TasksLength;
        tasks[TasksLength].function = &PhoneTask;
        iparams[TasksLength * task_params_length + 0] = i * 40;
        iparams[TasksLength * task_params_length + 1] = i * 40;
        iparams[TasksLength * task_params_length + 2] = 300;
        iparams[TasksLength * task_params_length + 3] = 300;
        iparams[TasksLength * task_params_length + 4] = 0;
        iparams[TasksLength * task_params_length + 5] = 20;
        iparams[TasksLength * task_params_length + 6] = 30;
        iparams[TasksLength * task_params_length + 7] = 5;
        iparams[TasksLength * task_params_length + 8] = 5;
        TasksLength++;
    }

    // SMS BTN
    char text7[] = "SMS\0";
    if (DrawButton(470, icon_y_pos - 20, 50, 40, 211 /2, 196, 187 /2, text7, 16, 32, 16, taskId) == TRUE) {
        tasks[TasksLength].priority = 0;
        tasks[TasksLength].taskId = TasksLength;
        tasks[TasksLength].function = &SMSTask;
        iparams[TasksLength * task_params_length + 0] = i * 40;
        iparams[TasksLength * task_params_length + 1] = i * 40;
        iparams[TasksLength * task_params_length + 2] = 300;
        iparams[TasksLength * task_params_length + 3] = 300;
        iparams[TasksLength * task_params_length + 4] = 0;
        iparams[TasksLength * task_params_length + 5] = 20;
        iparams[TasksLength * task_params_length + 6] = 30;
        iparams[TasksLength * task_params_length + 7] = 5;
        iparams[TasksLength * task_params_length + 8] = 5;
        TasksLength++;
    }
}

// Login Screen
int LoginScreen(int taskId) {
    VBEInfoBlock* VBE = (VBEInfoBlock*) VBEInfoAddress;

    int icon_x_pos = VBE->x_resolution ;
    int icon_y_pos = VBE->y_resolution-200;
    int i = iparams[taskId * task_params_length + 4];

    char text[] = "Login\0";
    char loginTitle[] = "LOGIN SCREEN\0";
    
    ClearScreen(181.0f / 255.0f * 16.0f, 232.0f / 255.0f * 32.0f, 255.0f / 255.0f * 16.0f);

    DrawRect(VBE->x_resolution / 4, VBE->y_resolution / 4, VBE->x_resolution / 2, VBE->y_resolution / 2, 16, 32, 16);
    DrawRect(VBE->x_resolution / 4, icon_y_pos, VBE->x_resolution / 2, 40, 5, 5, 5);

    DrawString(getArialCharacter, font_arial_width, font_arial_height, loginTitle, VBE->x_resolution / 4 + 100, VBE->y_resolution / 4 + 20, 255, 255, 255);
    if (DrawButton(VBE->x_resolution / 4, icon_y_pos, VBE->x_resolution / 2, 40, 255 / 4, 255 / 5, 255 / 4, text, 255, 255, 255, taskId) == TRUE) {
        tasks[TasksLength].priority = 0;
        tasks[TasksLength].function = &HeaderTask;
        tasks[TasksLength].taskId = TasksLength;
        iparams[TasksLength * task_params_length + 0] = 0;
        iparams[TasksLength * task_params_length + 1] = 0;
        iparams[TasksLength * task_params_length + 2] = VBE->x_resolution;
        iparams[TasksLength * task_params_length + 3] = 40;
        iparams[TasksLength * task_params_length + 4] = 1;
        TasksLength++;
        CloseTask(taskId);
    }
}