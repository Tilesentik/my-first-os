// Phone Program

int PhoneTask(int taskId) {

    int* r = &iparams[taskId * task_params_length + 4];
    int* g = &iparams[taskId * task_params_length + 5];
    int* b = &iparams[taskId * task_params_length + 6];
    
    // Window
    int closeClicked = DrawWindow(
        &iparams[taskId * task_params_length + 0],
        &iparams[taskId * task_params_length + 1],
        &iparams[taskId * task_params_length + 2],
        &iparams[taskId * task_params_length + 3],
        *r,
        *g,
        *b,
        &iparams[taskId * task_params_length + 9],
        taskId);

    int phone_x = iparams[taskId * task_params_length + 0];
    int phone_y = iparams[taskId * task_params_length + 1];


    if(closeClicked == TRUE)
    CloseTask(taskId);

    int phone_pos_x = 0;
    int phone_pos_y = 0;

    DrawRect(phone_x + 10, phone_y + 25, 280, 40, 153, 153, 102);
    char phone_digits[5];
    for (int i = 0; i <= 9; i++) {
        phone_digits[0] = i + '0';
        phone_digits[1] = '\0';

        if (i == 0) {
            phone_pos_x = 60;
            phone_pos_y = 170;
        }

        if (i == 1) {
            phone_pos_x = 0;
            phone_pos_y = 20;
        }

        if (i == 4) {
            phone_pos_x = 0;
            phone_pos_y = 70;

        } else if (i == 7) {
            phone_pos_x = 0;
            phone_pos_y = 120;
        }
        
        // APPEND DIGIT
        if (DrawButton(phone_x + phone_pos_x + 50, phone_y + phone_pos_y + 50, 50, 40, 153, 1, 50,
           phone_digits, 16, 32, 16, taskId
        ) == TRUE) {
            //DrawString(getArialCharacter, font_arial_width, font_arial_height, text3, x + 10, y + 35, 255, 255, 255);
        }
        phone_pos_x += 60;
    }
    phone_pos_x = 120;
    phone_pos_y = 170;
    char btn_pound[] = "#\0";
    if (DrawButton(phone_x + phone_pos_x + 50, phone_y + phone_pos_y + 50, 50, 40, 153, 1, 50,
           btn_pound, 16, 32, 16, taskId
    ) == TRUE) {
        //DrawString(getArialCharacter, font_arial_width, font_arial_height, text3, x + 10, y + 35, 255, 255, 255);
    }

    phone_pos_x = 0;
    phone_pos_y = 170;
    char btn_star[] = "*\0";
    if (DrawButton(phone_x + phone_pos_x + 50, phone_y + phone_pos_y + 50, 50, 40, 153, 1, 50,
           btn_star, 16, 32, 16, taskId
    ) == TRUE) {
        //DrawString(getArialCharacter, font_arial_width, font_arial_height, text3, x + 10, y + 35, 255, 255, 255);
    }

    phone_pos_x = 60;
    phone_pos_y = 170;
    char btn_call[] = "C\0";
    if (DrawButton(phone_x + phone_pos_x + 50, phone_y + phone_pos_y + 100, 50, 40, 153, 1, 50,
           btn_call, 16, 32, 16, taskId
    ) == TRUE) {
        //DrawString(getArialCharacter, font_arial_width, font_arial_height, text3, x + 10, y + 35, 255, 255, 255);
    }

    return 0;
}
