// Notes Program

int FilesTask(int taskId) {
    
    int* r = &iparams[taskId * task_params_length + 4];
    int* g = &iparams[taskId * task_params_length + 5];
    int* b = &iparams[taskId * task_params_length + 6];
    
    // Window
    int closeClicked = DrawWindow(
        &iparams[taskId * task_params_length + 0],
        &iparams[taskId * task_params_length + 1],
        &iparams[taskId * task_params_length + 2],
        &iparams[taskId * task_params_length + 3],
        *r,
        *g,
        *b,
        &iparams[taskId * task_params_length + 9],
        taskId
    );

    int x = iparams[taskId * task_params_length + 0];
    int y = iparams[taskId * task_params_length + 1];

    calc_x = x;
    calc_y = y;

    int width = iparams[taskId * task_params_length + 2];
    int height = iparams[taskId * task_params_length + 3];

    if(closeClicked == TRUE)
    CloseTask(taskId);

    char txt_home[] = "Home\0";
    char txt_documents[] = "Documents\0";
    char txt_downloads[] = "Downloads\0";
    char txt_music[] = "Music\0";
    char txt_photos[] = "Photos\0";

    int btn_r = 0;
    int btn_g = 0;
    int btn_b = 0;

    DrawRect(x + 0, y + 20, 100, 300, 153, 153, 102);
    if (DrawButton(x + 0, y + 20, 100, 40, 211/2, 196, 187/2, txt_home, 16, 32, 16, taskId) == TRUE) {}
    if (DrawButton(x + 0, y + 60, 100, 40, 211/2, 196, 187/2, txt_documents, 16, 32, 16, taskId) == TRUE) {}
    if (DrawButton(x + 0, y + 100, 100, 40, 211/2, 196, 187/2, txt_downloads, 16, 32, 16, taskId) == TRUE) {}
    if (DrawButton(x + 0, y + 140, 100, 40, 211/2, 196, 187/2, txt_music, 16, 32, 16, taskId) == TRUE) {}


    return 0;
}
