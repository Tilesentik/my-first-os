// Calculator Program


int calc_x;
int calc_y;

int temp = 0;

char text3[50];
char formula[50];
int formula_index = 0;

void strcpy(char *s, char *t) {
    while (*t) {
        *s = *t;
        s++;
        t++;
        }
    *s = *t;
}

int strcmp(const char *a, const char *b)
{
    while (*a && *a == *b) { ++a; ++b; }
    return (int)(unsigned char)(*a) - (int)(unsigned char)(*b);
}

void calculation() {
    DrawString(getArialCharacter, font_arial_width, font_arial_height, text3, calc_x + 10, calc_y + 35, 255, 255, 255);
}

int isdigit(char c) {
    if(c >= '0' && c <= '9') {
        return 1;
    }
    return 0;
}

int MathTask(int taskId) {
    int* r = &iparams[taskId * task_params_length + 4];
    int* g = &iparams[taskId * task_params_length + 5];
    int* b = &iparams[taskId * task_params_length + 6];
    
    // Window
    int closeClicked = DrawWindow(
        &iparams[taskId * task_params_length + 0],
        &iparams[taskId * task_params_length + 1],
        &iparams[taskId * task_params_length + 2],
        &iparams[taskId * task_params_length + 3],
        *r,
        *g,
        *b,
        &iparams[taskId * task_params_length + 9],
        taskId);

    int x = iparams[taskId * task_params_length + 0];
    int y = iparams[taskId * task_params_length + 1];

    calc_x = x;
    calc_y = y;

    int width = iparams[taskId * task_params_length + 2];
    int height = iparams[taskId * task_params_length + 3];

    if(closeClicked == TRUE)
    CloseTask(taskId);

    int btn_margin_left = 0;
    int btn_margin_top = 0;
    DrawRect(x + 10, y + 25, 280, 50, 153, 153, 102);
    calculation();
    char digits[5];
    
    // Placement Of Numbers
    for (int i = 0; i <= 9; i++) {
        digits[0] = i + '0';
        digits[1] = '\0';

        if (i == 0) {
            btn_margin_left = 60;
            btn_margin_top = 170;
        }

        if (i == 1) {
            btn_margin_left = 0;
            btn_margin_top = 120;
        }

        if (i == 4) {
            btn_margin_left = 0;
            btn_margin_top = 70;

        } else if (i == 7) {
            btn_margin_left = 0;
            btn_margin_top = 20;
            
        }

        // APPEND DIGIT
        
        if (DrawButton(x + 10 + btn_margin_left, y + 110 + btn_margin_top, 50, 40, 211 /2, 196, 187 /2,
           digits, 16, 32, 16, taskId
        ) == TRUE) {
            text3[temp] = digits[0];
            temp += 1;
            text3[temp] = '\0';
            
            formula[formula_index] = digits[0];
            formula_index += 1;
            //DrawString(getArialCharacter, font_arial_width, font_arial_height, text3, x + 10, y + 35, 255, 255, 255);
        }

        btn_margin_left += 60;
        /*
        if (DrawButton(x + 100, y + 20, 50, 20, 0, 32, 0,
                num_2, 16, 32, 16, taskId
        ) == TRUE) {
            *r = 16;
            *g = 31;
            *b = 16;
        }
        */
    }
    btn_margin_top = 20;
    btn_margin_left = 220;


    // Placement of operations
    char operation_list[] = { '+', '-', 'x', '/' };
    char btn_label[3];
    for (int i = 0; i <= 3; i++) {
        btn_label[0] = operation_list[i];
        btn_label[1] = '\0';

        if (DrawButton(x + 10 + btn_margin_left, y + 60 + btn_margin_top, 50, 40, 102 /2, 102, 51 /2,
            btn_label, 16, 32, 16, taskId
        ) == TRUE) {
            text3[temp] = btn_label[0];
            temp += 1;
            text3[temp] = '\0';
        }
        btn_margin_top += 50;
    }

    btn_margin_top = 80;
    btn_margin_left = 0;

    // Placement of Functions
    char function_list[] = { 'C', '%', '<' };
    for (int i = 0; i <= 2; i++) {
        btn_label[0] = function_list[i];
        btn_label[1] = '\0';

        if (DrawButton(x + 10 + btn_margin_left, y + btn_margin_top, 50, 40, 0, 32, 0,
            btn_label, 16, 32, 16, taskId
        ) == TRUE) {}
        btn_margin_left += 60;
    }

    btn_margin_top = 280;
    btn_margin_left = 0;

    // Placement of operations
    btn_label[0] = '.';
    btn_label[1] = '\0';
    if (DrawButton(x + 10 + btn_margin_left, y + btn_margin_top, 50, 40, 0, 32, 0,
        btn_label, 16, 32, 16, taskId
    ) == TRUE) {}

    btn_margin_left = 220;
    // Placement of Equal
    btn_label[0] = '=';
    btn_label[1] = '\0';
    
    char num1[10];
    int num1_index = 0;

    int num2_index = 0;
    char num2[10];

    char operation[10];
    strcpy ("none", operation);

    int num1_finnished = 0;
    if (DrawButton(x + 10 + btn_margin_left, y + btn_margin_top, 50, 40, 0, 32, 0,
        btn_label, 16, 32, 16, taskId
    ) == TRUE) {
        // Get array length
        size_t formula_length = sizeof(formula)/sizeof(formula[0]);

        for (int i = 0; i <= formula_length; i++) {
            if (num1_finnished != 1) {
                if (isdigit(formula[i])) {
                    num1[num1_index] = formula[i];
                    text3[0] = 'e';
                }
            }

            if (strcmp(operation, "none")) {
                text3[0] = 'a';
            }
        }
    }

    return 0;
}
