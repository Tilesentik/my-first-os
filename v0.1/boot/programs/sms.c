// SMS Program

int SMSTask(int taskId) {

    int* r = &iparams[taskId * task_params_length + 4];
    int* g = &iparams[taskId * task_params_length + 5];
    int* b = &iparams[taskId * task_params_length + 6];
    
    // Window
    int closeClicked = DrawWindow(
        &iparams[taskId * task_params_length + 0],
        &iparams[taskId * task_params_length + 1],
        &iparams[taskId * task_params_length + 2],
        &iparams[taskId * task_params_length + 3],
        *r,
        *g,
        *b,
        &iparams[taskId * task_params_length + 9],
        taskId);

    int x = iparams[taskId * task_params_length + 0];
    int y = iparams[taskId * task_params_length + 1];

    if(closeClicked == TRUE)
    CloseTask(taskId);

    char sms_txt_settings[] = "STG\0";
    char sms_txt_btn[] = "Send\0";

    DrawRect(x + 10, y + 250, 200, 40, 153, 153, 102);

    if (DrawButton(x + 250, y + 40, 50, 40, 153, 1, 50,
           sms_txt_settings, 16, 32, 16, taskId
    ) == TRUE) {
        //DrawString(getArialCharacter, font_arial_width, font_arial_height, text3, x + 10, y + 35, 255, 255, 255);
    }

    if (DrawButton(x + 250, y + 250, 50, 40, 153, 1, 50,
           sms_txt_btn, 16, 32, 16, taskId
    ) == TRUE) {
        //DrawString(getArialCharacter, font_arial_width, font_arial_height, text3, x + 10, y + 35, 255, 255, 255);
    }

    return 0;
}
