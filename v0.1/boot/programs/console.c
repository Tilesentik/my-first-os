int AppMenuTask(int taskId) {
    char app_text[] = "Console\0";
    VBEInfoBlock* VBE = (VBEInfoBlock*) VBEInfoAddress;
    // Console
    DrawRect(10, 100, VBE->x_resolution / 2, 210, 16, 32, 16);
    DrawRect(10, 100, VBE->x_resolution / 2, 20, 85/2, 86, 91/2);
    DrawString(getArialCharacter, font_arial_width, font_arial_height, app_text, 140, 100, 255, 255, 255);

    // Other
    DrawRect(350, 100, VBE->x_resolution / 5, 210, 16, 32, 16);
    DrawRect(350, 100, VBE->x_resolution / 5, 20, 85/2, 86, 91/2);

    // Other
    DrawRect(500, 100, VBE->x_resolution / 5, 210, 16, 32, 16);
    DrawRect(500, 100, VBE->x_resolution / 5, 20, 85/2, 86, 91/2);

    return 0; 
}
